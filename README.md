# *PROJECT_NAME* server

## Development
* First of all copy the `environment_DEFAULT.yml` file and rename its copy to `environment.yml`. There you can change the configuration as needed.

### Backend
* Run `docker-compuse up --build` in the root directory.
* To modify the server, just write the code and wait for the server to relaunch.
* This will launch your server in the browser [http://localhost:8000](http://localhost:8000)
